package com.companies.companiesapi.rest;

import com.companies.companiesapi.Service.CompanyService;
import com.companies.companiesapi.model.Company;
import com.companies.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
@CrossOrigin("*") //luban päringuid kõikjalt. Muidu on see domeenidega piiramiseks. Võib kirjutada domeeni siia välja (http://localhost/...
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository; //köök

    @Autowired
    private CompanyService companyService;

    @GetMapping("/hello/{name}")
    public String sayHello(@PathVariable("name") String namexx) {
        return("Hello " + namexx + "!");
    }

    @GetMapping
    public List<Company> GiveMeCopmanies() { //kelner
        return companyRepository.fetchCompanies();
    }

    @PostMapping
    public void addOrUpdateCompany (@RequestBody Company cc) {  //peaks olema "add or update"
        companyService.updateCompany(cc);
    }
    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable ("id") int id) {
        companyService.removeCompany(id);
    }

    @GetMapping("/{id}")
    public Company giveMeOneSingleCompany(@PathVariable("id") int id) {
        return companyRepository.fetchCompany(id);
    }
}
