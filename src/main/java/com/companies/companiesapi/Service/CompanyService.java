package com.companies.companiesapi.Service;

import com.companies.companiesapi.model.Company;
import com.companies.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository; //"külmkapp

    public void updateCompany(Company c) {
        Assert.notNull(c, "Company not found!"); //<- kontroll, kas see Company c on olemas. Kui ei, siis ..
        Assert.isTrue(c.getName() !=null, "Company name not specified!");
        Assert.isTrue(c.getLogo() !=null, "Logo not specified!");
        Assert.isTrue(c.getEmployees() > 0, "Company must have at least 1 employee!");

        if (c.getId() > 0 ) {
            companyRepository.updateCompany(c);
        } else {
            companyRepository.addCompany(c);
        }
    }

    public void removeCompany (int id) {
        Assert.isTrue(id > 0, "ID must be a positive integer!");
        Assert.notNull(companyRepository.fetchCompany(id), "ID not found!");
        companyRepository.removeCompany(id);

    }
}
