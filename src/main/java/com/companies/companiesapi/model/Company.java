package com.companies.companiesapi.model;

public class Company { // Copmany klass vajalik selleks, et DB tõmmata kirjed ja teha need objektideks.
    private int id;
    private String name;
    private String logo;
    private int employees;

    // getterid-setterid peavad alati olema sprinboodi jaoks olemas.
    // default constructor on vaja teha kindlasti isegi kui enda oma on ka tehtud.
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogo() {
        return logo;
    }

    public int getEmployees() {
        return employees;
    }

    public Company() { //default constructor on vaja springbooti jaoks teha, isegi kui enda oma on tehtud ka (praegu all)
    }

    public Company(int id, String name, String logo, int employees) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.employees = employees;
    }
}
