package com.companies.companiesapi.repository;

import com.companies.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate; //bean'ide teema

    public List<Company> fetchCompanies() {
        return jdbcTemplate.query("select * from company", (row, number) -> {
            return new Company(
                    row.getInt("id"), row.getString("name"),
                    row.getString("logo"), row.getInt("employees")
            );
        });
    }
    public void addCompany (Company xx) {
        jdbcTemplate.update("insert into company (`name`, `logo`, `employees`) values (?, ?, ?)",
                xx.getName(), xx.getLogo(), xx.getEmployees()); // ?-placeholder.
    }

    public void updateCompany(Company xx) {
        jdbcTemplate.update(("update company set `name` = ?, `logo` = ?, `employees` = ? where id = ?"),
                xx.getName(), xx.getLogo(), xx.getEmployees(), xx.getId());
    }

    public void removeCompany(int id) {
        jdbcTemplate.update("delete from company where id = ?", id);
    }

    public Company fetchCompany (int id) {
        List<Company> companies = jdbcTemplate
                .query("select * from company where id = ?",
                new Object[]{id},
                (row, number) -> {
            return new Company(
                    row.getInt("id"), row.getString("name"),
                    row.getString("logo"), row.getInt("employees"));
                });
        if (companies.size() > 0) {
            return companies.get(0);
        } else {
            return null;
        }
    }
}
